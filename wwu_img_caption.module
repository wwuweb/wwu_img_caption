<?php

/**
 * @file
 *
 * WWU Image Captions
 */

/**
 * Helper function to return the first result of preg_match().
 *
 * @param string $pattern
 *          The expression to be evaluated against.
 * @param string $subject
 *          The string to be evaluated.
 * @return The array of matches found, or any empty array on failure.
 */
function _wwu_img_caption_regex($pattern, $subject) {
  $matches = array();

  preg_match_all($pattern, $subject, $matches);

  return $matches;
}

/**
 * Implements hook_libraries_info().
 */
function wwu_img_caption_libraries_info() {
  $libraries = array();

  if (!libraries_detect('simple_html_dom')) {
    $libraries['simple_html_dom'] = array(
      'name' => 'PHP Simple HTML DOM Parser',
      'vendor url' => 'http://simplehtmldom.sourceforge.net/',
      'download url' => 'http://iweb.dl.sourceforge.net/project/simplehtmldom/simplehtmldom/1.5/simplehtmldom_1_5.zip',
      'version arguments' => array(
        'file' => 'simple_html_dom.php',
        'pattern' => '@version\s+([0-9]+.[0-9]+)@',
        'lines' => 100,
        'cols' => 50
      ),
      'files' => array(
        'php' => array(
          'simple_html_dom.php'
        )
      )
    );
  }

  return $libraries;
}

/**
 * Implements hook_filter_info().
 */
function wwu_img_caption_filter_info() {
  $filters['wwu_img_caption'] = array(
    'title' => t('Image Captions'),
    'description' => t('Every inline image with a title attribute will be given a caption containing the title text of the image.'),
    'process callback' => '_wwu_img_caption_filter_caption_process'
  );

  return $filters;
}

/**
 * Image caption filter process callback.
 *
 * Filtering operation is performed here. Each content image with a title
 * attribute set is replaced with
 */
function _wwu_img_caption_filter_caption_process($text, $filter, $format) {
  $library = libraries_load('simple_html_dom');

  // Return immediately if the required library files are missing, and display
  // an error.
  if (!$library['installed'] || !$library['loaded'] || !class_exists('simple_html_dom')) {
    drupal_set_message(t('The Simple HTML DOM library could not be loaded. Image caption processing failed.'), 'error', FALSE);

    return $text;
  }

  // CREATE DOM OBJECT //
  $dom = new simple_html_dom();

  // LOAD HTML //
  $dom->load($text);

  // PARSE IMAGES //
  foreach ($dom->find('img[class*=caption], img[class*=wwu-caption]') as $image) {
    /*
     * SETUP
     */

    // MODULE CLASS //
    $moduleClass = "wwu-img-caption";

    // REGEX PATTERNS //
    $widthExp = "/(^|\s)width:\s*\d+[A-Za-z]*;?/";
    $heightExp = "/(^|\s)height:\s*\d+[A-Za-z]*;?/";
    $floatExp = "/float:\s*(left|right);?/";
    $marginExp = "/margin(-(top|right|bottom|left))?:\s*(\d+[A-Za-z]*\s?)+;?/";

    // IMAGE ATTRIBUTES //
    $imageTitle = html_entity_decode(html_entity_decode($image->alt));
    $imageStyle = $image->style;
    $imageClasses = explode(' ', trim($image->class));

    // IMAGE PROPERTIES //
    $matches = _wwu_img_caption_regex($widthExp, $imageStyle);
    $imageWidthProp = array_shift($matches[0]);

    $matches = _wwu_img_caption_regex($heightExp, $imageStyle);
    $imageHeightProp = array_shift($matches[0]);

    $matches = _wwu_img_caption_regex($floatExp, $imageStyle);
    $imageFloatProp = array_shift($matches[0]);

    $matches = _wwu_img_caption_regex($marginExp, $imageStyle);
    $imageMarginProp = trim(implode(' ', $matches[0]));

    /*
     * ELEMENTS
     */

    // ASIDE //
    $asideStyle = array();

    if (!empty($imageFloatProp)) {
      array_push($asideStyle, $imageFloatProp);
    }

    if (!empty($imageMarginProp)) {
      array_push($asideStyle, $imageMarginProp);
    }

    $asideStyle = trim(implode(' ', $asideStyle));
    $asideTag = "<aside class=\"{$moduleClass}-aside\" style=\"{$asideStyle}\">";

    // FIGURE //
    $figureTag = "<figure class=\"{$moduleClass}-figure\">";

    // IMAGE //
    if (!empty($imageWidthProp)) {
      $matches = _wwu_img_caption_regex("/\d+/", $imageWidthProp);
      $width = array_shift($matches[0]);
      $image->width = $width;
      $imageStyle = preg_replace($widthExp, '', $imageStyle);
    }

    if (!empty($imageHeightProp)) {
      $matches = _wwu_img_caption_regex("/\d+/", $imageHeightProp);
      $height = array_shift($matches[0]);
      $image->height = $height;
      $imageStyle = preg_replace($heightExp, '', $imageStyle);
    }

    if (!empty($imageFloatProp)) {
      $imageStyle = preg_replace($floatExp, '', $imageStyle);
    }

    if (!empty($imageMarginProp)) {
      $imageStyle = preg_replace($marginExp, '', $imageStyle);
    }

    array_push($imageClasses, "{$moduleClass}-image");
    $image->class = trim(implode(' ', $imageClasses));
    $image->style = trim($imageStyle);

    // FIGCAPTION //
    $figcaptionStyle = array();

    if (!empty($imageWidthProp)) {
      array_push($figcaptionStyle, $imageWidthProp);
    }

    $figcaptionStyle = trim(implode(' ', $figcaptionStyle));
    $figcaptionTag = "<figcaption class=\"{$moduleClass}-figcaption\" style=\"{$figcaptionStyle}\">";

    /*
     * UPDATE HTML
     */

    $image->outertext = "{$asideTag}{$figureTag}{$image->outertext}{$figcaptionTag}{$imageTitle}</figcaption></figure></aside>";
  }

  // SAVE HTML //
  $text = $dom->save();

  return $text;
}
